package com.springrest.springrest.services;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springrest.springrest.dao.CourseDao;
import com.springrest.springrest.entities.Course;
@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	private CourseDao courseDao;
	@Override
	public List<Course> getCourses() {
		// TODO Auto-generated method stub
		return courseDao.findAll();
	}
	@Override
	public Course addCourse(Course course) {
		courseDao.save(course); 
		return course;
	}
	@Override
    public Optional<Course> getCourse(long id) {
		return courseDao.findById(id);
    }
	@Override
	public Course modifyCourse(Course course) {
		courseDao.save(course);
		return course;
	}
	@Override
	public String deleteCourse(long courseID) {
		Course entity = courseDao.getById(courseID);
		courseDao.delete(entity);
		return "Deleted successfully";
	}
	
}
