package com.springrest.springrest.services;

import java.util.List;
import java.util.Optional;

import com.springrest.springrest.entities.Course;
public interface CourseService {
	public List<Course> getCourses();
	public Course addCourse(Course course);
	public Optional<Course> getCourse(long id);
	public Course modifyCourse(Course course);
	public String deleteCourse(long id);
}
