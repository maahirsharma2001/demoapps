package com.springrest.springrest.controller;
import java.util.List;
import java.util.Optional;

import org.springframework.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.springrest.springrest.entities.Course;
import com.springrest.springrest.services.CourseService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
@RestController
public class MyController {
	@Autowired
	private CourseService courseservice;
	
	@CrossOrigin
	@GetMapping("/courses")
	public List<Course> getCourse(){
		return this.courseservice.getCourses();
	}
	
	@CrossOrigin
	@GetMapping("/courses/{courseID}")
	public Optional<Course> getc(@PathVariable long courseID) {
		return this.courseservice.getCourse(courseID);
	}
	
	@CrossOrigin
	@PostMapping("/courses")
	public Course addCourse(@RequestBody Course course) {
		return this.courseservice.addCourse(course);
	}
	
	@CrossOrigin
	@PutMapping("/courses")
	public Course modifyCourse(@RequestBody Course course) {
		return this.courseservice.modifyCourse(course);
	}
	
	@CrossOrigin
	@DeleteMapping("/courses/{courseID}")
	public String deleteCourse(@PathVariable long courseID) {
		return this.courseservice.deleteCourse(courseID);
	}
}
